Address: Franklin, TN 37069, USA

Avery Swartz grew up in Franklin, Tennessee as the oldest of three siblings. As a child, she enjoyed being coached by her father on her recreational soccer team and playing in the neighborhood with her friends. She also learned the value of responsibility and time management through her various jobs she worked in high school. As soon as Avery received her driver’s license she got a job at a local restaurant balancing work, school, and sports successfully. 
Throughout high school Avery played Division 1 travel soccer and was the editor and chief of the school’s yearbook her senior year. Avery graduated from high school with honors and went on to attend Auburn University. Her freshman year of college she was accepted into the nationally-ranked Interior Design program.
She is now a senior in college and loves getting lemonade at Toomer’s corner with her friends and attending Auburn football games in the fall. In her free time Avery stays active by playing intramural soccer and going to cycling classes. While at school Avery has an extremely hard work ethic and enjoys being a part of a team. After graduation, she hopes to move to a new city and get a job at a hospitality design firm. Eventually, Avery hopes to be able to travel the world designing low income housing for underprivileged families. This has been a dream of hers for a long time, growing up in a close-knit, loving family Avery hopes to be able to provide a loving home for every family in need.

Instagram:  https://www.instagram.com/_aves06/   

LinkedIn:  https://www.linkedin.com/in/avery-s-4b59aa1b4/

Facebook:  https://www.facebook.com/profile.php?id=100008432837431
